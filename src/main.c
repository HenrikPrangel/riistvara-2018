#define __ASSERT_USE_STDERR
#include <assert.h>
#include <string.h>
#include <time.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "hmi.h"
#include "print_helper.h"
#include "cli_microrl.h"

#include "../lib/eriks_freemem/freemem.h"
#include "../lib/andygock_avr_uart/uart.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/helius_microrl/microrl.h"

#define UART_STATUS_MASK 0x00FF

microrl_t rl;
microrl_t *prl = &rl;

static inline void init_con_uart0(void)
{
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart0_puts_p(PSTR(C_MSG "\r\n"));
    uart0_puts_p(PSTR(USER_NAME "\r\n"));
    uart0_puts_p(PSTR("Use backspace to delete entry and enter to confirm.\r\n"));
    // Call init with ptr to microrl instance and print callback
    microrl_init(prl, uart0_puts);
    // Set callback for execute
    microrl_set_execute_callback(prl, cli_execute);
}

static inline void init_con_uart1(void)
{
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_puts_p(PSTR(C_E_MSG));
    uart1_puts_p(PSTR(VER_FW));
    uart1_puts_p(PSTR(VER_LIBC));
}

static inline void init_leds(void)
{
    DDRA |= _BV(DDA1);
    DDRA |= _BV(DDA3);
    DDRA |= _BV(DDA5);
    DDRB |= _BV(DDB7);
    PORTB &= ~_BV(PORTB7);
}

static inline void init_lcd(void)
{
    lcd_init();
    lcd_goto(LCD_ROW_1_START);
    lcd_puts_P(PSTR(USER_NAME));
}

static inline void init_sys_timer(void)
{
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= _BV(CS12); // fCPU/256
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}

static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if (prev_time != now) {
        //Print uptime to uart1
        ltoa(now, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        //Toggle LED
        PORTA ^= _BV(LED_GREEN);
        prev_time = now;
    }
}

void main(void)
{
    init_con_uart0();
    init_con_uart1();
    init_leds();
    init_lcd();
    init_sys_timer();
    sei(); // Enable all interrupts. Needed for UART!!

    while (1) {
        heartbeat();
        // CLI commands are handled in cli_execute()
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
    }
}

/* System timer ISR */
ISR(TIMER1_COMPA_vect)
{
    system_tick();
}
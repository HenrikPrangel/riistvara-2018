#include <avr/pgmspace.h>
#ifndef HMI_H
#define HMI_H

#define C_MSG "-- Console Started --"
#define C_E_MSG "\r\n-- Error Console started --\r\n"
#define MSG "Enter Month name first letter >"
#define UART_BAUD 9600
#define VER_FW "Version: " FW_VERSION " built on: " __DATE__ " "__TIME__"\r\n"
#define VER_LIBC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__ "\r\n"
#define BLINK_DELAY_MS 100
#define LED_RED PORTA1 // Arduino Mega digital pin 23
#define LED_GREEN PORTA3 // Arduino Mega digital pin 25
#define LED_BLUE PORTA5 // Arduino Mega digital pin 27
#define BANNER_ROWS 5
#define NAME_MONTH_COUNT 12
#define USER_NAME "Henrik Prangel"

extern PGM_P const name_month[] PROGMEM;
extern PGM_P const banner[] PROGMEM;

#endif /* UART_H */

#include "hmi.h"
#include <avr/pgmspace.h>

const char M1[] PROGMEM = "January";
const char M2[] PROGMEM = "February";
const char M3[] PROGMEM = "March";
const char M4[] PROGMEM = "April";
const char M5[] PROGMEM = "May";
const char M6[] PROGMEM = "June";
const char M7[] PROGMEM = "July";
const char M8[] PROGMEM = "August";
const char M9[] PROGMEM = "September";
const char M10[] PROGMEM = "October";
const char M11[] PROGMEM = "November";
const char M12[] PROGMEM = "December";

PGM_P const name_month[NAME_MONTH_COUNT] PROGMEM = {
    M1,
    M2,
    M3,
    M4,
    M5,
    M6,
    M7,
    M8,
    M9,
    M10,
    M11,
    M12
};

const char B1[] PROGMEM = "__________________";
const char B2[] PROGMEM = "|                |";
const char B3[] PROGMEM = "|     IGAV       |";
const char B4[] PROGMEM = "|      BANNER    |";
const char B5[] PROGMEM = "|________________|";

PGM_P const banner[BANNER_ROWS] PROGMEM = {
    B1,
    B2,
    B3,
    B4,
    B5
};